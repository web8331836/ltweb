<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký Tân Sinh Viên</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="form-container">
        <form>
            <div class="name">
                <label for="full_name">Họ và Tên:</label>
                <input type="text" id="full_name" name="full_name" required><br>
            </div>

            <div class="gender">
                <label for="gender">Giới tính:</label>
                <?php
                $genders = ["Nam", "Nữ"];
                for ($i = 0; $i < count($genders); $i++) {
                    echo '<input type="radio" name="gender" value="' . $i . '"> ' . $genders[$i] . '<br>';
                }
                ?>
            </div>


            <label for="department">Phân khoa:</label>
            <select id="department" name="department">
                <option value="" selected>--Chọn phân khoa--</option>
                <option value="MAT">Khoa học máy tính</option>
                <option value="KDL">Khoa học vật liệu</option>
            </select><br>
    </div>

    <button type="submit" id="register-button">Đăng ký</button>
    </form>
</body>

</html>