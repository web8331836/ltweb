<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <style>
        body {
            width: 300px;
            margin: 0 auto;
            padding: 20px 50px;
            border: 2px solid #000;
            border-radius: 10px;
            background-color: #fff;
            text-align: center;
        }

        .time {
            height: 30px;
            padding: 5px;
            margin-bottom: 10px;
            background-color: #f2f2f2ff;
        }

        form {
            width: 300px;
            margin: 0 auto;
            text-align: left;
        }

        label {
            font-weight: bold;
            width: 50%;
            background-color: #007bff;
            border: 1px solid #0067d4;
            padding: 6px;
        }

        label[for="username"],
        input[type="text"],
        label[for="password"],
        input[type="password"] {
            color: #fff;
            width: 50%;
            height: 27px;
            border: 1px solid #007bff;
            margin-bottom: 10px;
        }

        input[type="submit"] {
            background-color: #007bff;
            color: #fff;
            padding: 10px 20px;
            border: 1px solid #007bff;
            cursor: pointer;
            border-radius: 5px;
        }
    </style>
</head>

<body>
    <div class="time">
        <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $daysInVietnamese = array(
            'Monday' => 'Thứ Hai',
            'Tuesday' => 'Thứ Ba',
            'Wednesday' => 'Thứ Tư',
            'Thursday' => 'Thứ Năm',
            'Friday' => 'Thứ Sáu',
            'Saturday' => 'Thứ Bảy',
            'Sunday' => 'Chủ Nhật'
        );

        $currentDay = date('l');
        $currentDayInVietnamese = $daysInVietnamese[$currentDay];

        $currentDateTime = date('d/m/Y H:i:s');
        echo "Bây giờ là: $currentDayInVietnamese, $currentDateTime";
        ?>

    </div>

    <form method="POST" action="process_login.php">
        <label for="username">Tên người dùng:</label>
        <input type="text" id="username" name="username" required><br>

        <label for="password">Mật khẩu:</label>
        <input type="password" id="password" name="password" required><br>

        <input type="submit" value="Đăng nhập">
    </form>
</body>

</html>