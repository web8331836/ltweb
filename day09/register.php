<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký Tân Sinh Viên</title>
    <style>
        body {
            max-width: 400px;
            margin: 0 auto;
            padding: 20px;
            border: 2px solid #007bff;
            border-radius: 10px;
            background-color: #fff;
            text-align: center;
        }

        label {
            font-weight: bold;
            color: #fff;
            background-color: #02ac10;
            padding: 6px;
            display: inline-block;
            width: 90px;
        }

        .form-container {
            text-align: left;
        }

        .form-section {
            margin-bottom: 20px;
        }

        span {
            color: red;
        }

        input[type="text"],
        select,
        input[type="file"] {
            height: 27px;
            border: 1px solid #007bff;
        }

        .gender {
            display: flex;
        }

        .button-container {
            text-align: center;
        }

        #student_image {
            border: none;
        }

        #register-button {
            background-color: #02ac10;
            color: #fff;
            padding: 10px 20px;
            border: 1px solid #007bff;
            cursor: pointer;
            border-radius: 5px;
        }
    </style>
</head>

<body>
    <div class="form-container">
        <form id="registration-form" method="post" action="confirm.php">
            <div class="form-section">
                <label for="full_name">Họ và Tên <span>*</span></label>
                <input type="text" id="full_name" name="full_name" required>
            </div>

            <div class="form-section gender">
                <label>Giới tính <span>*</span></label>
                <input type="radio" name="gender" value="Nam" required> Nam
                <input type="radio" name="gender" value="Nữ" required> Nữ
            </div>

            <div class="form-section">
                <label for="department">Phân khoa <span>*</span></label>
                <select id="department" name="department" required>
                    <option value="" selected>--Chọn phân khoa--</option>
                    <option value="MAT">Khoa học máy tính</option>
                    <option value="KDL">Khoa học vật liệu</option>
                </select>
            </div>

            <div class="form-section">
                <label for="dob">Ngày sinh <span>*</span></label>
                <input type="text" id "dob" name="dob" placeholder="dd/mm/yyyy" required>
            </div>

            <div class="form-section">
                <label for="address">Địa chỉ</label>
                <input type="text" id="address" name="address">
            </div>

            <div class="form-section">
                <label for="student_image">Hình ảnh</label>
                <input type="file" id="student_image" name="student_image">
            </div>

            <div class="button-container">
                <button type="submit" id="register-button">Đăng ký</button>
            </div>
        </form>
        <div id="error-messages"></div>
    </div>

    <script>
        $(document).ready(function() {
            $("#registration-form").submit(function(event) {
                event.preventDefault();
                $("#error-messages").html("");

                var fullName = $("#full_name").val();
                var gender = $("input[name='gender']:checked").val();
                var department = $("#department").val();
                var dob = $("#dob").val();
                var address = $("#address").val();
                var errors = [];

                if (fullName === "") {
                    errors.push("Hãy nhập tên.");
                }

                if (department === "") {
                    errors.push("Hãy chọn phân khoa.");
                }

                if (dob === "") {
                    errors.push("Hãy nhập ngày sinh.");
                } else {
                    var dobRegex = /^\d{2}\/\d{2}\/\d{4}$/;
                    if (!dob.match(dobRegex)) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                    }
                }

                if (errors.length > 0) {
                    var errorMessage = "<ul>";
                    for (var i = 0; i < errors.length; i++) {
                        errorMessage += "<li>" + errors[i] + "</li>";
                    }
                    errorMessage += "</ul>";
                    $("#error-messages").html(errorMessage);
                } else {
                    document.getElementById("registration-form").submit();
                }
            });
        });
    </script>
</body>

</html>