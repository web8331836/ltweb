<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tên trang web của bạn</title>
</head>
<style>
    .container {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: 100vh;
        width: 100vw;
        margin-top: 20px;
        background-color: white;
    }

    .input_name {
        background-color: #4F81BD;
        color: white;
        padding: 10px;
        margin: 5px;
        border-radius: 0;
        width: 100px;
        text-align: center;
        display: inline;
        border: 2px solid #41719C;
    }

    .entering {
        padding: 10px;
        border-radius: 0;
        width: 150px;
        margin: 5px 20px;
        border: 2px solid #41719C;
    }

    form {
        display: flex;
        padding-left: 20px;
    }

    .button-container {
        background-color: #4F81BD;
        color: white;
        cursor: pointer;
        margin-top: 20px;
        border: 2px solid #41719C;
    }

    #nonsubmitButton {
        width: 120px;
        height: 38px;
        padding: 10px;
        margin-top: 20px;
        margin-left: 8px;
        border-radius: 5px;
        background-color: #4F81BD;
        border: 3px solid #41719C;
    }

    #submitButton {
        width: 75px;
        height: 30px;
        border-radius: 5px;
        background-color: #4F81BD;
        border: 3px solid #41719C;
        margin-left: 90px;
        margin-top: 55px;
    }

    .title {
        margin-right: 90px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 10px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    th {
        background-color: white;
        font-weight: bold;
    }

    #tableButton {
        padding: 10px 20px;
        margin: 5px;
        background-color: #92B1D6;
        border: 3px solid #4F729D;
    }

    th:not(:first-child),
    th:not(:first-child) {
        margin-left: 2px;
    }

    th:nth-child(3) {
        margin-right: 20px;
    }
</style>

<body>
    <div class="container">
        <form>
            <label for="inputName" class="input_name">Khoa</label>
            <input type="text" name="inputName" class="entering" required><br><br>
        </form>
        <form>
            <label for="inputKeyword" class="input_name">Từ Khóa</label>
            <input type="text" name="inputKeyword" class="entering" required><br><br>
        </form>
        <button class="button-container" id="nonSubmitButton">Tìm kiếm</button>
        <button class="button-container" id="submitButton">Thêm</button>
        <p class="title">Số sinh viên tìm thấy: XXX</p>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    global $connect;
                    include 'database.php';
                    $sql = "SELECT fullname, department FROM students";
                    $result = $connect->query($sql);

                    $i = 1;
                    while ($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $i . "</td>";
                        echo "<td>" . $row["fullname"] . "</td>";
                        echo "<td>" . $row["department"] . "</td>";
                        echo "<td>";
                        echo '<button class="button-container" id="tableButton">Xóa</button>';
                        echo '<button class="button-container" id="tableButton">Sửa</button>';
                        echo "</td>";
                        echo "</tr>";
                        $i++;
                    }

                    $connect->close();
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
<script>
    function goToPage(pageUrl) {
        window.location.href = pageUrl;
    }

    if (document.getElementById('submitButton') !== null) {
        document.getElementById('submitButton').addEventListener('click', function() {
            goToPage("register.php");
        });
    }
</script>

</html>