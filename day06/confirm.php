<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận thông tin đăng ký</title>
</head>

<style>
    body {
        max-width: 400px;
        margin: 0 auto;
        padding: 20px;
        border: 2px solid #007bff;
        border-radius: 10px;
        background-color: #fff;
        text-align: center;
    }

    .form_container {
        text-align: left;
    }

    strong {
        font-weight: bold;
        color: #fff;
        background-color: #02ac10;
        padding: 6px;
        margin-right: 30px;
        display: inline-block;
        width: 100px;
    }

    #confirm-button {
        background-color: #02ac10;
        color: #fff;
        padding: 10px 20px;
        border: 1px solid #007bff;
        cursor: pointer;
        border-radius: 5px;
    }
</style>

<body>
    <div class="form_container">
        <?php
        include("database.php");

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $full_name = $_POST["full_name"];
            $gender = $_POST["gender"];
            $department = $_POST["department"];
            $dob = $_POST["dob"];
            $address = $_POST["address"];
            $student_image = $_POST["student_image"];

            echo "<p><strong>Họ và Tên:</strong> $full_name</p>";
            echo "<p><strong>Giới tính:</strong> $gender</p>";
            echo "<p><strong>Phân khoa:</strong> $department</p>";
            echo "<p><strong>Ngày sinh:</strong> $dob</p>";
            echo "<p><strong>Địa chỉ:</strong> $address</p>";
            echo "<p><strong>Hình ảnh:</strong> $student_image</p>";

            // Sử dụng prepared statement để chèn dữ liệu vào bảng students
            $stmt = $conn->prepare("INSERT INTO students (full_name, gender, department, dob, address, student_image) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssss", $full_name, $gender, $department, $dob, $address, $student_image);

            if ($stmt->execute()) {
                echo "Dữ liệu đã được lưu vào cơ sở dữ liệu.";
            } else {
                echo "Lỗi: " . $stmt->error;
            }

            $stmt->close();
        }

        $conn->close();



        // if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //     $full_name = $_POST["full_name"];
        //     $gender = $_POST["gender"];
        //     $department = $_POST["department"];
        //     $dob = $_POST["dob"];
        //     $address = $_POST["address"];
        //     $student_image = $_POST["student_image"];

        //     echo "<p><strong>Họ và Tên:</strong> $full_name</p>";
        //     echo "<p><strong>Giới tính:</strong> $gender</p>";
        //     echo "<p><strong>Phân khoa:</strong> $department</p>";
        //     echo "<p><strong>Ngày sinh:</strong> $dob</p>";
        //     echo "<p><strong>Địa chỉ:</strong> $address</p>";
        //     echo "<p><strong>Hình ảnh:</strong> $student_image</p>";
        // }
        ?>
    </div>

    <div class="button-container">
        <button type="submit" id="confirm-button">Xác nhận</button>
    </div>
</body>

</html>