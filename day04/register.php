<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký Tân Sinh Viên</title>
    <style>
        body {
            width: 300px;
            margin: 0 auto;
            padding: 20px 50px;
            border: 2px solid #000;
            border-radius: 10px;
            border-color: #007bff;
            background-color: #fff;
            text-align: center;
        }

        label {
            font-weight: bold;
            color: #fff;
            background-color: #02ac10;
            border: 1px solid #0067d4;
            padding: 6px;
            display: inline-block;
            width: 100px;
        }

        .form-container {
            width: 300px;
            margin: 0 auto;
            text-align: left;
        }

        span {
            color: red;
        }

        .name,
        .gender,
        #department,
        #dob,
        #address {
            margin-bottom: 20px;
        }

        input[type="text"] {
            height: 27px;
            border: 1px solid #007bff;
        }

        .gender {
            align-items: center;
            display: flex;
        }

        #department {
            height: 33px;
            border: 1px solid #007bff;
        }

        #dob {
            margin-bottom: 20px;
        }

        #address {
            margin-bottom: 20px;
        }

        #register-button {
            background-color: #02ac10;
            color: #fff;
            padding: 10px 20px;
            border: 1px solid #007bff;
            cursor: pointer;
            border-radius: 5px;
        }
    </style>
</head>

<body>
    <div class="form-container">
        <form id="registration-form">
            <div class="name">
                <label for="full_name">Họ và Tên <span>*</span>:</label>
                <input type="text" id="full_name" name="full_name" required><br>
            </div>

            <div class="gender">
                <label>Giới tính <span>*</span>:</label>
                <input type="radio" name="gender" value="Nam" required> Nam
                <input type="radio" name="gender" value="Nữ" required> Nữ<br>
            </div>

            <div class="department">
                <label for="department">Phân khoa <span>*</span>:</label>
                <select id="department" name="department" required>
                    <option value="" selected>--Chọn phân khoa--</option>
                    <option value="MAT">Khoa học máy tính</option>
                    <option value="KDL">Khoa học vật liệu</option>
                </select><br>
            </div>

            <div class="dob">
                <label for="dob">Ngày sinh <span>*</span>:</label>
                <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy" required><br>
            </div>

            <div class="address">
                <label for="address">Địa chỉ: </label>
                <input type="text" id="address" name="address"><br>
            </div>

            <button type="submit" id="register-button">Đăng ký</button>
        </form>
        <div id="error-messages"></div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#registration-form").submit(function(event) {
                event.preventDefault(); // Ngăn form gửi đi

                // Xóa thông báo lỗi trước đó
                $("#error-messages").html("");

                // Lấy giá trị từ form
                var fullName = $("#full_name").val();
                var gender = $("input[name='gender']:checked").val();
                var department = $("#department").val();
                var dob = $("#dob").val();
                var address = $("#address").val();

                // Mảng lưu trữ thông báo lỗi
                var errors = [];

                if (fullName === "") {
                    errors.push("Hãy nhập tên.");
                }

                if (department === "") {
                    errors.push("Hãy chọn phân khoa.");
                }

                if (dob === "") {
                    errors.push("Hãy nhập ngày sinh.");
                } else {
                    var dobRegex = /^\d{2}\/\d{2}\/\d{4}$/;
                    if (!dob.match(dobRegex)) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                    }
                }

                // Hiển thị thông báo lỗi
                if (errors.length > 0) {
                    var errorMessage = "<ul>";
                    for (var i = 0; i < errors.length; i++) {
                        errorMessage += "<li>" + errors[i] + "</li>";
                    }
                    errorMessage += "</ul>";
                    $("#error-messages").html(errorMessage);
                } else {
                    // Nếu không có lỗi, gửi form
                    $("#registration-form").unbind('submit').submit();
                }
            });
        });
    </script>
</body>

</html>